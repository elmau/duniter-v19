# duniter-v19

Archivos para levantar un nodo Duniter v1.9.0.

Basado en:
https://forum.monnaie-libre.fr/t/installer-facilement-duniter-1-9-gva/26923

Para ver el proceso detallado, mira nuestro wiki en:
[https://wiki.cuates.net/books/duniter-v190](https://wiki.cuates.net/books/duniter-v190)


### Requerimientos previos

* VPS con al menos 4 GB de RAM.
* Subdominio apuntando a la IP de este VPS.
* Acceso root a este VPS.
* Docker CE y docker compose.
* Let's Encrypted y los certificados del subdominio ya solicitados.


Aportaciones:

```
A5DdXxCKPw3QKWVdDVs7CzkNugNUW1sHu5zDJFWxCU2h
```
